#!/usr/bin/env python
# coding: utf-8
from calculator.simplecalculator import SimpleCalculator as SimpleCalculator
import unittest
import logging


"""
Simple test module for calculator module test

"""




class AdditionTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_addition_two_integers(self):
        result = self.calculator.sum(5, 6)
        self.assertEqual(result, 11)
        logging.warning('test_addition_two_integer ok warn')  # will print a message to the console
        logging.info('test_addition_two_integer ok inf')  # will not print anything

    def test_addition_integer_string(self):
        result = self.calculator.sum(5, "6")
        self.assertEqual(result, "ERROR")

    def test_addition_negative_integers(self):
        result = self.calculator.sum(-5, -6)
        self.assertEqual(result, -11)
        self.assertNotEqual(result, 11)


class SubtractionTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_substration_two_integers(self):
        result = self.calculator.substract(6, 5)
        self.assertEqual(result, 1)

    def test_subtraction_integer_string(self):
        result = self.calculator.substract(5, "6")
        self.assertEqual(result, "ERROR")

    def test_subtraction_negative_integers(self):
        result = self.calculator.substract(-5, -6)
        self.assertEqual(result, 1)
        self.assertNotEqual(result, -11)        


class MultiplicationTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_multiplication_two_integers(self):
        result = self.calculator.multiply(6, 5)
        self.assertEqual(result, 30)

    def test_multiplication_integer_string(self):
        result = self.calculator.multiply(5, "6")
        self.assertEqual(result, "ERROR")

    def test_multiplication_negative_integers(self):
        result = self.calculator.multiply(-5, -6)
        self.assertEqual(result, 30)
        self.assertNotEqual(result, -30)         


class DivisionTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()

    def test_division_two_integers(self):
        result = self.calculator.divide(30, 5)
        self.assertEqual(result, 6)

    def test_division_integer_string(self):
        result = self.calculator.divide(5, "6")
        self.assertEqual(result, "ERROR")

    def test_division_negative_integers(self):
        result = self.calculator.divide(-30, -6)
        self.assertEqual(result, 5)
        self.assertNotEqual(result, -5)  

    def test_divide_by_zero_exception(self):
        with self.assertRaises(ZeroDivisionError):
            self.calculator.divide(10, 0)


if __name__ == "__main__":
    unittest.main()



 
